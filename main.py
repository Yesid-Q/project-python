from flask import render_template

from app.main import createApp
from config.settings import Config
from app.routers.auth_router import auth
from app.routers.home_router import home
from app.routers.event_router import event_route
from app.routers.user_router import user

app = createApp(config=Config)

app.register_blueprint(home)
app.register_blueprint(auth)
app.register_blueprint(event_route)
app.register_blueprint(user)

@app.errorhandler(404)
def error404(e):
    return render_template('layout/404.html', e=e)

if __name__ == '__main__':
  
    app.run(port=8000)
