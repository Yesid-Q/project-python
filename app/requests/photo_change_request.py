import os
from flask import flash, redirect, request, url_for
from functools import wraps

from app.models.user_model import UserModel

UPLOAD_EXTE = set(['jpg', 'png', 'gif'])

def photo_change_request_decorator(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        if request.method == 'GET':
            return redirect(url_for('home.index'))

        if request.method == 'POST':
            uuid_user = request.args.get('user')
            user_exists = UserModel.get_or_none(UserModel.id == uuid_user)

            if user_exists is None:
                flash('No se puede cambiar la foto a un usuario que no existe.', 'error')
                errors = True

            errors:bool = False
            file = request.files['photo']
            if not file is None:
                if not file.filename.rsplit('.', 1)[1] in UPLOAD_EXTE:
                    flash('El archivo no es una imagen.', 'error')
                    errors = True
            if errors:
                return redirect(f'/user/edit?user={uuid_user}')
        return f(*args, **kwargs)
    return wrapped   