from flask import flash, session, redirect, url_for
from functools import wraps


def login_required_decorator(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not 'auth' in session:
            flash('Debes iniciar sesión.','error')
            return redirect(url_for('auth.index'))
        return f(*args, **kwargs)
    return wrapper