from flask import flash, session, redirect, url_for
from functools import wraps


def not_login_decorator(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if 'auth' in session:
            flash('Esta la sesión activa.', 'error')
            return redirect(url_for('home.index'))
        return f(*args, **kwargs)
    return wrapper