from flask import render_template, request
from werkzeug.security import check_password_hash
from functools import wraps

from app.validators.length_validator import LengthValidate
from app.validators.password_validator import PasswordValidate
from app.validators.email_validator import EmailValidate

from app.models.user_model import UserModel

def user_request_decorator(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        errors_context={
            'error_first': '',
            'error_last': '',
            'error_age': '',
            'error_profession': '',
            'error_email': '',
            'error_password': '',
            'error_new': '',
            'error_veri_pass': ''
        }
        uuid_user = request.args.get('user')
        user = UserModel.select( UserModel.id, UserModel.photo, UserModel.first_name,
            UserModel.last_name, UserModel.email,
            UserModel.profession, UserModel.age, UserModel.password).dicts().where(UserModel.id == uuid_user).get()

        if request.method == 'GET':
            context={
                'id': user['id'],
                'first_name': user['first_name'],
                'last_name': user['last_name'],
                'age': user['age'],
                'photo': user['photo'],
                'profession': user['profession'],
                'email': user['email'],
                'password': '',
                'new_pass': '',
                'veri_pass': ''
            }
            return render_template('user/edit.html', **context, **errors_context)
        else:
            errors:bool = False
           
            first_name = LengthValidate(request.form.get('first_name'), 3, 50)
            last_name = LengthValidate(request.form.get('last_name'), 3, 50)
            age = request.form.get('age') or 0
            profession = LengthValidate(request.form.get('profession'), 5, 100)
            email = EmailValidate(request.form.get('email'), user['id'])

            new_pass = request.form.get('new_pass')
            veri_pass = request.form.get('veri_pass')

            password = request.form.get('password')
            change_pass = PasswordValidate(request.form.get('new_pass'), request.form.get('veri_pass'))
            
            if new_pass or veri_pass:
                if password.strip() == '':
                    errors_context['error_password'] = ['La contraseña es requerida.']
                    errors = True
                else:
                    if not check_password_hash(user['password'], password):
                        errors_context['error_password'] = ['No es la contraseña registrada.']
                        errors = True
                    if not change_pass.validate_password():
                        errors = True
                        errors_context['error_new'] = change_pass.get_errors()
                    if not change_pass.validate_veri_pass():
                        errors = True
                        errors_context['error_veri_pass'] = change_pass.get_error_veri()
            
            if not first_name.validate():
                errors = True
                errors_context['error_first'] = first_name.get_errors()
            if not last_name.validate():
                errors = True
                errors_context['error_last'] = last_name.get_errors()
            if not profession.validate():
                errors = True
                errors_context['error_profession'] = profession.get_errors()
            if not email.validate():
                errors = True
                errors_context['error_email'] = email.get_errors()
            if int(age) < 15 or int(age) > 100:
                errors = True
                errors_context['error_age'] = 'Edad esta por fuera del rango.'
            
            if errors:
                context = {
                    'id': user['id'],
                    'first_name': request.form.get('first_name'),
                    'last_name': request.form.get('last_name'),
                    'age': request.form.get('age'),
                    'profession': request.form.get('profession'),
                    'photo': user['photo'],
                    'email': request.form.get('email'),
                    'password': request.form.get('password'),
                    'new_pass': request.form.get('new_pass'),
                    'veri_pass': request.form.get('veri_pass')
                }
                return render_template('user/edit.html', **context, **errors_context)
            
        return f(*args, **kwargs)
    return wrapper