from flask import  render_template, request
from functools import wraps

from app.models.event_model import EventModel
from app.validators.length_validator import LengthValidate
from app.validators.date_hour_validator import DateHourValidate

UPLOAD_EXTE = set(['jpg', 'png', 'gif'])

def event_request_decorator(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        context_errors = {
            'error_title': '',
            'error_place': '',
            'error_day': '',
            'error_hour': '',
            'error_image': '',
            'error_description': ''
        }
        if request.method == 'GET':
            context = {
                'title': '',
                'description': '',
                'event_day': '',
                'event_hour': '',
                'event_place': ''
            }
            return render_template('event/created.html', **context, **context_errors)
        if request.method == 'POST':
            errors:bool = False
            file = request.files['event_photo'] or None
            if not file is None:
                if not file.filename.rsplit('.', 1)[1] in UPLOAD_EXTE:
                    context_errors['error_image'] = ['El archivo no es una imagen.']
                    errors = True
            else:
                context_errors['error_image'] = ['Se requiere una imagen.']
                errors = True

            title = LengthValidate(request.form.get('title'), 5, 100)
            description = LengthValidate(request.form.get('description'), 10, 500)
            event_date = DateHourValidate(request.form.get('event_day'), request.form.get('event_hour'))
            event_place = LengthValidate(request.form.get('event_place'), 5, 255)
            
            if not title.validate():
                context_errors['error_title'] = title.get_errors()
                errors = True
            else:
                if EventModel.get_or_none(EventModel.title == request.form.get('title').lower()):
                    context_errors['error_title'] = ['Existe un evento con ese titulo.']
                    errors = True

            if not description.validate():
                context_errors['error_description'] = description.get_errors()
                errors = True
            if not event_place.validate():
                context_errors['error_place'] = event_place.get_errors()
                errors = True
            if not event_date.validate_date():
                context_errors['error_day'] = event_date.get_error_date()
                errors = True
            if not event_date.validate_hour():
                context_errors['error_hour'] = event_date.get_error_hour()
                errors = True

            if errors:
                context = {
                    'title': request.form.get('title'),
                    'description': request.form.get('description'),
                    'event_day': request.form.get('event_day'),
                    'event_hour': request.form.get('event_hour'),
                    'event_place': request.form.get('event_place')
                }
                return render_template('event/created.html', **context, **context_errors)

        return f(*args, **kwargs)
    return wrapped