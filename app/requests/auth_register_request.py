import os
from typing import Dict
from flask import flash, render_template, request
from functools import wraps

from app.validators.length_validator import LengthValidate
from app.validators.password_validator import PasswordValidate
from app.validators.email_validator import EmailValidate

UPLOAD_EXTE = set(['jpg', 'png', 'gif'])


def auth_register_decorator(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        errors_context:Dict={
            'error_first': '',
            'error_last': '',
            'error_age': '',
            'error_profession': '',
            'error_email': '',
            'error_password': '',
            'error_veri_pass': '',
            'error_photo': ''
        }
        if request.method == 'GET':
            context={
                'first_name': '',
                'last_name': '',
                'age': '',
                'profession': '',
                'email': '',
                'password': '',
                'veri_pass': ''
            }
            return render_template('auth/register.html', **context, **errors_context)
        else:
            errors:bool = False
            file = request.files['photo'] or None
            if not file is None:
                if not file.filename.rsplit('.', 1)[1] in UPLOAD_EXTE:
                    errors_context['error_photo'] = ['El archivo no es una imagen.']
                    errors = True

            first_name = LengthValidate(request.form.get('first_name'), 3, 50)
            last_name = LengthValidate(request.form.get('last_name'), 3, 50)
            age = request.form.get('age') or 0
            profession = LengthValidate(request.form.get('profession'), 5, 100)
            email = EmailValidate(request.form.get('email'))
            password = PasswordValidate(request.form.get('password'), request.form.get('veri_pass'))
            
            if not first_name.validate():
                errors = True
                errors_context['error_first'] = first_name.get_errors()
            if not last_name.validate():
                errors = True
                errors_context['error_last'] = last_name.get_errors()
            if not profession.validate():
                errors = True
                errors_context['error_profession'] = profession.get_errors()
            if not email.validate():
                errors = True
                errors_context['error_email'] = email.get_errors()
            if not password.validate_password():
                errors = True
                errors_context['error_password'] = password.get_errors()
            if not password.validate_veri_pass():
                errors = True
                errors_context['error_veri_pass'] = password.get_error_veri()
            if int(age) < 15 or int(age) > 100:
                errors = True
                errors_context['error_age'] = 'Edad esta por fuera del rango.'
            
            if errors:
                context = {
                    'first_name': request.form.get('first_name'),
                    'last_name': request.form.get('last_name'),
                    'age': request.form.get('age'),
                    'profession': request.form.get('profession'),
                    'email': request.form.get('email'),
                    'password': request.form.get('password'),
                    'veri_pass': request.form.get('veri_pass')
                }
                return render_template('auth/register.html', **context, **errors_context)
            
        return f(*args, **kwargs)
    return wrapper