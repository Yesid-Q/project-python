from flask import Flask

from config.base_model import database
from app.models.event_model import EventModel
from app.models.user_model import UserModel

def createApp(config):
    app: Flask = Flask(__name__)

    app.config.from_object(config())

    database.create_tables(models=[UserModel, EventModel])

    app.config['UPLOAD_FOLDER'] = 'app/static/uploads/'

    return app  
