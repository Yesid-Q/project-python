from peewee import BigAutoField, BooleanField, CharField, DateField, ForeignKeyField, TextField, TimeField
from flask_peewee.utils import slugify

from config.base_model import BaseModel
from app.models.user_model import UserModel

class EventModel(BaseModel):
    id = BigAutoField(column_name='id', primary_key=True )
    title = CharField(column_name='title', max_length=100, unique=True )
    slug = CharField(column_name='slug', max_length=255 )
    description = TextField(column_name='description' )
    event_day = DateField(column_name='event_day', formats=['%Y-%m-%d'] )
    event_hour = TimeField(column_name='event_hour', formats=['%H:%M'] )
    event_place = CharField(column_name='event_place', max_length=255 )
    event_photo = CharField(column_name='event_photo', max_length=255 )
    is_published = BooleanField(column_name='is_public', default=False)
    user = ForeignKeyField(UserModel, backref='events')

    class Meta:
        table_name = 'events'

    def save(self, *args, **kwargs):
        self.title = self.title.lower()
        self.description = self.description.lower()
        self.event_place = self.event_place.lower()
        self.slug = slugify(self.title)
        super(EventModel, self).save(*args, **kwargs)