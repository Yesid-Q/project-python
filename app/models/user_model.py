from peewee import CharField, DateTimeField, IntegerField, UUIDField, uuid

from werkzeug.security import generate_password_hash
import datetime

from config.base_model import BaseModel

class UserModel( BaseModel ):
    id = UUIDField(column_name='id', primary_key=True )
    first_name = CharField(column_name='first_name', max_length=50 )
    last_name = CharField(column_name='last_name', max_length=50 )
    age = IntegerField(column_name='age', null=True )
    profession = CharField(column_name='profession', max_length=100, null=True )
    photo = CharField(column_name='photo', max_length=255, null=True )
    email = CharField(column_name='email', max_length=100, unique=True )
    password = CharField(column_name='password', max_length=200 )
    created_at = DateTimeField(column_name='created', default=datetime.datetime.now)
    updated_at = DateTimeField(column_name='update', default=datetime.datetime.now)

    class Meta:
        table_name = 'users'

    def make_password(self):
        self.password = generate_password_hash(self.password, method='sha256')

    def save(self,  *args, **kwargs):
        self.id = uuid.uuid4()
        self.first_name = self.first_name.lower()
        self.last_name = self.last_name.lower()
        self.profession = self.profession.lower()
        self.email = self.email.lower()
        self.make_password()
        super(UserModel, self).save(*args, **kwargs)
