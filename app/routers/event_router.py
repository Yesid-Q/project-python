import os
import datetime
from flask import Blueprint, flash, request, session, redirect, url_for, render_template, current_app as app
from werkzeug.utils import secure_filename


from app.requests.login_required import login_required_decorator
from app.requests.event_request import event_request_decorator
from app.requests.event_edit_request import event_edit_request_decorator
from app.models.event_model import EventModel
from app.models.user_model import UserModel

event_route = Blueprint('event_route', __name__, url_prefix='/event' )

@event_route.route('/', methods=['GET'])
@login_required_decorator
def index():
    events = EventModel.select().where( (EventModel.event_day >= datetime.date.today()) & (EventModel.is_published == 1))

    return render_template('event/index.html', data=events)

@event_route.route('/get', methods=['GET'])
@login_required_decorator
def getevent():
    slug = request.args.get('slug')

    event = EventModel.get_or_none(EventModel.slug == slug)
    if event is None:
        flash('El evento no existe.','error')
        return redirect(url_for('home.index'))
   
    event = EventModel.select(
        EventModel.title, EventModel.slug, EventModel.description, EventModel.event_photo, EventModel.event_day, EventModel.event_hour,
        EventModel.event_place, EventModel.is_published, UserModel.photo, UserModel.first_name, UserModel.id, UserModel.last_name, UserModel.profession
    ).join(UserModel, on=(EventModel.user == UserModel.id)).dicts().where(EventModel.slug == slug).get()

    if event['is_published']:
        return render_template('event/event.html', **event)
    else:
        flash('El evento buscado no esta disponible', 'error')
        return redirect(url_for('event_route.index'))


@event_route.route('/created', methods=['GET', 'POST'])
@login_required_decorator
@event_request_decorator
def store():
    file = request.files['event_photo']
        
    filename = secure_filename(file.filename)
    file.save(os.path.join(app.config['UPLOAD_FOLDER']+ 'events/',filename))
    
    user_id = session['auth']

    data = {
        'title': request.form.get('title'),
        'description': request.form.get('description'),
        'event_day': request.form.get('event_day'),
        'event_hour': request.form.get('event_hour'),
        'event_place': request.form.get('event_place'),
        'event_photo': f'events/{filename}',
        'is_published': int(request.form.get('is_published')),
        'user_id': user_id
    }
    _ = EventModel.create(**data)

    flash('Un evento nuevo fue creado.', 'info')

    return redirect(url_for('event_route.store'))
    

@event_route.route('/edit', methods=['GET','POST'])
@login_required_decorator
@event_edit_request_decorator
def event_edit():
    slug = request.args.get('slug')
    event = EventModel.select().where(EventModel.slug == slug).dicts().get()

    file = request.files['event_photo']

    if file:
        os.remove(app.config['UPLOAD_FOLDER']+event['event_photo'])
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER']+ 'events/',filename))
        data:dict = {
            'title': request.form.get('title'),
            'description': request.form.get('description'),
            'event_day': request.form.get('event_day'),
            'event_hour': request.form.get('event_hour'),
            'event_place': request.form.get('event_place'),
            'event_photo': f'events/{filename}',
            'is_published': int(request.form.get('is_published'))
        }
    else:
        data:dict = {
            'title': request.form.get('title'),
            'description': request.form.get('description'),
            'event_day': request.form.get('event_day'),
            'event_hour': request.form.get('event_hour'),
            'event_place': request.form.get('event_place'),
            'is_published': int(request.form.get('is_published'))
        }
    
    _ = EventModel.update(**data).where(EventModel.id == event['id']).execute()

    flash('Un evento nuevo fue actualizado.', 'info')

    return redirect(url_for('home.index'))

@event_route.route('/deleted', methods=['GET'])
@login_required_decorator
def delete_event():
    slug = request.args.get('slug') or None

    if slug is None:
        return redirect(url_for('home.index'))

    event = EventModel.get_or_none(EventModel.slug == slug)

    if event is None:
        return redirect(url_for('home.index'))

    event = EventModel.select().where(EventModel.slug == slug).dicts().get()

    if str(event['user']) != str(session['auth']):
        flash('El evento que intenta eliminar no lo programo.', 'error')
        return redirect(url_for('home.index'))

    os.remove(app.config['UPLOAD_FOLDER']+event['event_photo'])
    EventModel.delete().where(EventModel.id == event['id']).execute()
    flash('El evento se elimino de manera exitosa.', 'info')
    return redirect(url_for('home.index'))
