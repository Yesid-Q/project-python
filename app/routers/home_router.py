import datetime
from flask import Blueprint, render_template, session, request

from app.requests.login_required import login_required_decorator
from app.models.user_model import UserModel
from app.models.event_model import EventModel

home = Blueprint('home', __name__, url_prefix='')

@home.route('/home')
@home.route('/')
@login_required_decorator
def index():
    auth = session['auth']

    user = UserModel.select(
        UserModel.id, UserModel.photo, UserModel.first_name,
        UserModel.last_name, UserModel.email,
        UserModel.profession, UserModel.age
    ).dicts().where(UserModel.id == auth).get()

    date1 = request.args.get('date1') or None
    date2 = request.args.get('date2') or None

    hour1 = request.args.get('hour1') or None
    hour2 = request.args.get('hour2') or None

    description = request.args.get('description') or None
    place = request.args.get('place') or None
    
    if date1 and date2:
        events = (EventModel.select().where( ((EventModel.user == auth) & (EventModel.event_day >= datetime.date.today())) 
                & (EventModel.event_day.between(date1, date2 ))))
    elif hour1 and hour2:
        events = (EventModel.select().where( ((EventModel.user == auth) & (EventModel.event_day >= datetime.date.today())) 
                & (EventModel.event_hour.between(hour1, hour2 ))))
    elif description:
        events = (EventModel.select().where( ((EventModel.user == auth) & (EventModel.event_day >= datetime.date.today())) 
                & (EventModel.description ** f'%{description}%' )))
    elif place:
        events = (EventModel.select().where( ((EventModel.user == auth) & (EventModel.event_day >= datetime.date.today())) 
                & (EventModel.event_place ** f'%{place}%' )))
    else:
        events = (EventModel.select().where( (EventModel.user == auth) & (EventModel.event_day >= datetime.date.today()) ))
        
    return render_template('home/index.html', data=events, **user)

@home.route('/past')
@login_required_decorator
def past():
    auth = session['auth']

    user = UserModel.select(
        UserModel.id, UserModel.photo, UserModel.first_name,
        UserModel.last_name, UserModel.email,
        UserModel.profession, UserModel.age
    ).dicts().where(UserModel.id == auth).get()

    
    date1 = request.args.get('date1') or None
    date2 = request.args.get('date2') or None

    hour1 = request.args.get('hour1') or None
    hour2 = request.args.get('hour2') or None

    description = request.args.get('description') or None
    place = request.args.get('place') or None

    if date1 and date2:
        events = (EventModel.select().where( ((EventModel.user == auth) & (EventModel.event_day < datetime.date.today())) 
                & (EventModel.event_day.between(date1, date2 ))))
    elif hour1 and hour2:
        events = (EventModel.select().where( ((EventModel.user == auth) & (EventModel.event_day < datetime.date.today())) 
                & (EventModel.event_hour.between(hour1, hour2 ))))
    elif description:
        events = (EventModel.select().where( ((EventModel.user == auth) & (EventModel.event_day < datetime.date.today())) 
                & (EventModel.description ** f'%{description}%' )))
    elif place:
        events = (EventModel.select().where( ((EventModel.user == auth) & (EventModel.event_day < datetime.date.today())) 
                & (EventModel.event_place ** f'%{place}%' )))
    else:
        events = (EventModel.select().where( (EventModel.user == auth) & (EventModel.event_day < datetime.date.today()) ))
    
    return render_template('home/index.html', data=events, **user)