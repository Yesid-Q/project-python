import os
from flask import Blueprint, flash, request, session, redirect, url_for, render_template, current_app as app
from werkzeug.security import check_password_hash
from werkzeug.utils import secure_filename

from app.models.user_model import UserModel
from app.requests.not_login import not_login_decorator
from app.requests.auth_register_request import auth_register_decorator

auth = Blueprint('auth', __name__, url_prefix='/auth')

@auth.route('/')
@not_login_decorator
def index():
    return render_template('auth/index.html')


@auth.route('/register', methods=['GET','POST'])
@not_login_decorator
@auth_register_decorator
def register():
    file = request.files['photo']

    if file:
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER']+ 'users/',filename))
    else:
        filename = 'default.png'

    data:dict = {
        'first_name': request.form.get('first_name'),
        'last_name': request.form.get('last_name'),
        'age': request.form.get('age'),
        'profession': request.form.get('profession'),
        'photo': f'users/{filename}',
        'email': request.form.get('email'),
        'password': request.form.get('password')
    }

    user = UserModel.create(**data)
    flash(f"Registro exitoso, Bienvenido {user.first_name}.", "info")
    session['auth'] = str(user.id)
    return redirect(url_for('home.index'))
        
        
@auth.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return redirect(url_for('auth.index'))

    email: str = request.form.get('email')
    password: str = request.form.get('password')
    
    user:UserModel = UserModel.get_or_none(UserModel.email == email)
  
    if user is None or not check_password_hash(user.password, password):
        flash('Verifique sus credenciales.', 'error')
        return redirect(url_for('auth.index'))

    session['auth'] = str(user.id)
    flash(f'Bienvenido {user.first_name}', 'info')
    return redirect(url_for('home.index'))


@auth.route('/logout', methods=['GET'])
def logout():
    if 'auth' in session:
        session.pop('auth', None)
        flash('Sesion cerrada, vuelva pronto.', 'info')
    return redirect(url_for('auth.index'))