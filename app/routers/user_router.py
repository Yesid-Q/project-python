import os
from flask import Blueprint, flash, redirect, render_template, request, url_for, current_app as app
from flask.globals import session
from werkzeug.utils import secure_filename

from app.models.user_model import UserModel
from app.models.event_model import EventModel

from app.requests.login_required import login_required_decorator
from app.requests.user_request import user_request_decorator
from app.requests.photo_change_request import photo_change_request_decorator

user = Blueprint('user', __name__, url_prefix='/user')

@user.route('/')
@login_required_decorator
def index():

    users = UserModel.select(UserModel.id, UserModel.photo, UserModel.first_name,
        UserModel.last_name, UserModel.email,
        UserModel.profession, UserModel.age)

    return render_template('user/index.html', data=users)

@user.route('/edit', methods=['GET','POST'])
@login_required_decorator
@user_request_decorator
def update():
    uuid_user = request.args.get('user')
    
    data:dict = {
        'first_name': request.form.get('first_name'),
        'last_name': request.form.get('last_name'),
        'age': request.form.get('age'),
        'profession': request.form.get('profession'),
        'email': request.form.get('email')
    }

    _ = UserModel.update(**data).where(UserModel.id == uuid_user).execute()
    flash("Actualizacion exitoso.", "info")
    return redirect(url_for('home.index'))

@user.route('/change-photo', methods=['GET', 'POST'])
@login_required_decorator
@photo_change_request_decorator
def change_photo():
    uuid_user = request.args.get('user')
    
    photo = request.files['photo']
    user = UserModel.select(UserModel.photo).dicts().where(UserModel.id == uuid_user).get()
    if user['photo'] == 'users/default.png':
        filename = secure_filename(photo.filename)
    else:
        os.remove(app.config['UPLOAD_FOLDER']+user['photo'])
        filename = secure_filename(photo.filename)
    
    photo.save(os.path.join(app.config['UPLOAD_FOLDER']+ 'users/',filename))
    UserModel.update({UserModel.photo : f'users/{filename}'} ).where(UserModel.id == uuid_user).execute()
    flash('Se actualizo la foto de perfil.', 'info')
    
    return redirect(url_for('home.index'))

@user.route('/deleted-user', methods=['GET'])
@login_required_decorator
def delete_user():
    auth = session['auth']
    events = EventModel.select(EventModel.event_photo).where(EventModel.user == str(auth))

    for event in events:
        os.remove(app.config['UPLOAD_FOLDER']+event.event_photo)

    EventModel.delete().where(EventModel.user == str(auth)).execute()
    user = UserModel.select(UserModel.photo).dicts().where(UserModel.id == str(auth)).get()
    os.remove(app.config['UPLOAD_FOLDER']+user['photo'])
    UserModel.delete().where(UserModel.id == str(auth)).execute()
    session.pop('auth', None)
    flash('Cuenta ELIMINADA, te extrañaremos.', 'info')
    return redirect(url_for('auth.index'))