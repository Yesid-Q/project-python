import re
from typing import List


class PasswordValidate(object):

    def __init__(self, password:str, veri_pass:str):
        self.password: str = password
        self.veri_pass: str = veri_pass
        self.valid: bool = True
        self.errors: List[str] = []
        self.veri_errors: List[str] = []

    def validate_password(self)->bool:
        self.valid = True
        if self.password.strip() =='':
            self.errors.append('El campo es requerido.')
            self.valid = False
            return self.valid
        if len(self.password.strip()) < 8:
            self.valid = False
            self.errors.append('La contraseña debe tener entre 8 a 16 caracteres.')
        else:
            if not re.search('[A-Z]', self.password):
                self.valid = False
                self.errors.append('La contraseña debe tener al menos 1 mayuscula.')
            if not re.search('[a-z]', self.password):
                self.valid = False
                self.errors.append('La contraseña debe tener al menos 1 minuscula.')
            if not re.search('[0-9]', self.password):
                self.valid = False
                self.errors.append('La contraseña debe tener al menos 1 numero.')

        return self.valid

    def validate_veri_pass(self)->bool:
        self.valid = True
        if self.veri_pass.strip() == '':
            self.veri_errors.append('El campo es requerido.')
            self.valid = False
        else:
            if self.veri_pass != self.password:
                self.valid = False
                self.veri_errors.append('Las contraseñas no coinciden.')
        return self.valid

    def get_error_veri(self)->str:
        return self.veri_errors

    def get_errors(self)->List:
        return self.errors
    