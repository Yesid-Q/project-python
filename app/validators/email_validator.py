import re
from typing import List
from app.models.user_model import UserModel

class EmailValidate(object):

    def __init__(self, email:str, id:str or None = None):
        self.data: str = email
        self.id: int or None = id
        self.valid: bool = True
        self.errors: List[str] = []

    def validate(self)->bool:
        self.valid = True
        if self.data.strip() =='':
            self.errors.append('El campo es requerido.')
            self.valid = False
            return self.valid
        if not re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,}$', self.data):
            self.errors.append('Formato de la correo incorrecto.')
            self.valid = False
        if self.id:
            if UserModel.get_or_none(UserModel.email == self.data, UserModel.id != self.id):
                self.errors.append('El correo se encuentra registrado por otro usuario.')
                self.valid = False
        else:
            if UserModel.get_or_none(UserModel.email == self.data):
                self.errors.append('El correo se encuentra registrado.')
                self.valid = False
        return self.valid

    def get_errors(self)->List:
        return self.errors