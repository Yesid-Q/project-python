from typing import List

class LengthValidate(object):
    
    def __init__(self, data:str, min:int, max:int):
        self.data: str = data
        self.min: int = min
        self.max: int = max
        self.errors: List[str] = []
        self.valid: bool = True
    
    def validate(self)->bool:
        if self.data.strip() == '':
            self.errors.append('El campo es requerido.')
            self.valid = False
            return self.valid
        if len(self.data.strip()) < self.min:
            self.errors.append(f'El campo debe tener {self.min} minimo.')
            self.valid = False
        if len(self.data.strip()) > self.max:
            self.errors.append(f'El campo debe tener {self.max} maximo.')
            self.valid = False
        return self.valid

    def get_errors(self)->List:
        return self.errors