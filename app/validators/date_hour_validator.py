from typing import List

from datetime import datetime

class DateHourValidate(object):

    def __init__(self, date, hour):
        self.date = date
        self.hour = hour
        self.valid: bool = True
        self.error_date: List[str] = []
        self.error_hour: List[str] = []

    def validate_date(self)->bool:
        self.valid = True
        try:
            day = datetime.strptime(self.date, '%Y-%m-%d')
            if day < datetime.today():
                self.error_date.append('El evento no se puede crear con una fecha atrasada.')
                self.valid = False
        except:
            self.error_date.append('La fecha del evento es obligatoria.')
            self.valid = False
        return self.valid

    def validate_hour(self)->bool:
        self.valid = True
        try:
            datetime.strptime(self.hour, '%H:%M')
        except:
            self.error_hour.append('La hora del evento es obligatoria.')
            self.valid = False
        return self.valid

    def get_error_date(self)-> List:
        return self.error_date

    def get_error_hour(self)-> List:
        return self.error_hour
    