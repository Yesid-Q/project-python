Instalacion:

1. Abrir la consola en la carpeta.
2. Ejecutar el comando 
    start.bat
3. Este comando creara un entorno virtual con la libreria virutalenv
4. Ejecutar el comando
    install.bat
5. Este comando instala todas las librerias que requiere el programa
6. Editar el arquivo .env de la carpeta config
7.  DEBUG=true  //por defecto esta el verdadero
    SECRET_KEY=secret   //valor secreto que requiere el programa
    ENV=development //entorno de desarrollo

    DATABASE_HOST=127.0.0.1 //host de la base de datos
    DATABASE_PORT=3306  //puerto de la base de datos
    DATABASE_USER=root  //usuario de la base de datos
    DATABASE_DB=project-python  //nombre de la base de datos
    DATABASE_PASS=  //contraseña de la base de datos

8. Despues de editar el archivo .env se puede ejecutar el programa con el comando
    python main.py

9. Si vuelve a probar el proyecto solo realize el comando
    .bat
    y el paso 8.