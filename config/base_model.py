import environ
from peewee import Model

env = environ.Env()
environ.Env.read_env()

if env.bool('DEBUG'):
    from peewee import MySQLDatabase
    database = MySQLDatabase(env.str('DATABASE_DB'), user=env.str('DATABASE_USER'), password=env.str('DATABASE_PASS'), host=env.str('DATABASE_HOST'), port=env.int('DATABASE_PORT'))
else:
    from peewee import PostgresqlDatabase
    database = PostgresqlDatabase(env.str('DATABASE_DB'), user=env.str('DATABASE_USER'), password=env.str('DATABASE_PASS'), host=env.str('DATABASE_HOST'), port=env.int('DATABASE_PORT'))


class BaseModel(Model):
    class Meta:
        database = database