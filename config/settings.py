import environ

env = environ.Env()
environ.Env.read_env()
class Config(object):
    SECRET_KEY:str = env.str('SECRET_KEY')
    DEBUG:bool = env.bool('DEBUG')
    ENV:str = env.str('ENV')

